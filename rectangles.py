"""
Implementation of Two Columns Heuristic algorithm
(http://www.devx.com/dotnet/Article/36005/0/page/2)
for the optimal placement of rectangles on a sheet of plywood in Python

input: minimum and maximum rectangle size, maximum plywood width,
       limits of the number of rectangles (must be integers!)

Published under CC0 license (public domain).
http://creativecommons.org/choose/zero/
"""

import random


def init_vars():
    global min_rect_size, max_rect_size, rect_count, plywood_width
    global right_column, coords_and_rects
    min_rect_size = 1
    max_rect_size = 10
    rect_count = random.randrange(10, 15)
    plywood_width = max_rect_size
    right_column = plywood_width // 2
    coords_and_rects = ''


def generate_rectangles(min_rect_size, max_rect_size, rect_count):
    '''generate list with rectangles'''
    list_rects = [(random.randrange(min_rect_size, max_rect_size),
                    random.randrange(min_rect_size, max_rect_size))
                    for i in range(rect_count)]
    return list_rects


def separate_rectangles(list_rects, plywood_width):
    '''Separate wide from narrow'''
    list_wide_rects = []
    list_narrow_rects = []
    for rectangle in list_rects:
        if rectangle[0] >= (plywood_width / 2) + 1:
            list_wide_rects.append(rectangle)
        else:
            list_narrow_rects.append(rectangle)
    return list_wide_rects, list_narrow_rects


def sort_wide(list_wide_rects):
    global coords_and_rects
    coordinate = [0, 0]
    for rectangle in list_wide_rects:
        coords_and_rects += str(coordinate) + str(rectangle) + '\n'
        coordinate[1] += rectangle[1]
    return coordinate


def sort_by_colums(coordinate, right_column, list_narrow_rects):
    '''conside the two columns and place rectangle to the column with smaller
    total height
    '''
    # sort narrow rectangles by height
    sorted_list = sorted(list_narrow_rects,
                         key=lambda height: height[1], reverse=True)

    global coords_and_rects
    y_left = coordinate[1]
    y_right = coordinate[1]
    for rectangle in sorted_list:
        if y_left <= y_right:
            coords_and_rects += str([0, y_left]) + str(rectangle) + '\n'
            y_left += rectangle[1]
        else:
            coords_and_rects += (str([right_column, y_right]) +
                                 str(rectangle) + '\n')
            y_right += rectangle[1]
    return sorted_list

init_vars()
print 'plywood width:', plywood_width
list_rects = generate_rectangles(min_rect_size, max_rect_size, rect_count)
print 'rectangles (x, y):', list_rects
list_wide_rects, list_narrow_rects = separate_rectangles(list_rects,
                                                         plywood_width)
print 'wide rectangles:', list_wide_rects
print 'narrow rectangles:', list_narrow_rects

coordinate = sort_wide(list_wide_rects)
sorted_list = sort_by_colums(coordinate, right_column, list_narrow_rects)
print coords_and_rects
print 'narrow rectangles, sorted by height:'
print 'placement of narrow rectangles \
[coordinates of top lefe point] (dimensions of rectangle)'
